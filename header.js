/*
  TODO:
  Find author of the following
*/

var rainbow_texts = document.getElementsByClassName("rainbow");

for (var element = 0; element < rainbow_texts.length; element++) {
    var string = rainbow_texts.item(element).innerHTML;
    rainbow_texts.item(element).innerHTML = ""; // Clear original


    for (var i = 0; i < string.length; i++) {
	// Separate each letter & put into text
        rainbow_texts.item(element).innerHTML += "<span style='--n:"+ (100 * i - 10000 + 'ms') + ";'>" + string[i] + "</span>";
    }
}

// Days since <2022-07-03>
var startdate = new Date("2022-07-03").getTime();
function calctime() {
	let len = new Date().getTime() - startdate;
	let y = Math.floor(len / (1000 * 60 * 60 * 24) / 365);
	let d = Math.floor(len / (1000 * 60 * 60 * 24) % 365);
	let h = Math.floor((len % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
	let m = Math.floor((len % (1000 * 60 * 60)) / (1000 * 60));
	let s = Math.floor((len % (1000 * 60)) / 1000);

    document.getElementById("timesince").innerHTML = `${y} years, ${d} days, ${h} hours, ${m} minutes, and ${s} seconds`
}
calctime();
let _ = setInterval(calctime , 1000);
